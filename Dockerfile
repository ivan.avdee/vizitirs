FROM python:3.9-slim

EXPOSE 5000

WORKDIR /app
COPY * /app

RUN pip3 install -r requirements.txt

RUN touch config.py &&\
	echo "DB_HOST = 'mysql'" >> config.py &&\
	echo "DB_USER = '$DB_USER'" >> config.py &&\
	echo "DB_PASS = '$DB_PASS'" >> config.py &&\
	echo "DB_DATABASE = '$DB_DATABASE'" >> config.py &&\
	echo "USE_PORT = '$USE_PORT'" >> config.py